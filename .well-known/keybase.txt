==================================================================
https://keybase.io/mmh
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://henrymike.com
  * I am mmh (https://keybase.io/mmh) on keybase.
  * I have a public key ASCla07MWukZH7fm0-eaNBOK6EbFSb2UO8gZyiTjP6vSfwo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120d518f256e9120c580b69d2b0f3b36ea61a6b9368f609e6b0d8e0ceb9f7854e250a",
      "host": "keybase.io",
      "kid": "0120a56b4ecc5ae9191fb7e6d3e79a34138ae846c549bd943bc819ca24e33fabd27f0a",
      "uid": "84719a696a02037d66cf2b697f7cf919",
      "username": "mmh"
    },
    "merkle_root": {
      "ctime": 1529631707,
      "hash": "f8d2dcc786e337356dbb13d4717e6346d7909b71c62d4b9a9d9d370340a91fa91d961b91b87990a1bc804511902cd00c80d5abe35d2c9541550e40f9b6a57807",
      "hash_meta": "4902e110a0ec749d60b12bfa967240c49d5e526f25256b6319abcc404b4026c3",
      "seqno": 3124024
    },
    "service": {
      "entropy": "RK8ow43GpOWIqXsk1umzpcqj",
      "hostname": "henrymike.com",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "2.3.0"
  },
  "ctime": 1529631725,
  "expire_in": 504576000,
  "prev": "b934bc511482584dc2f18a178e45ff61d332da98a7a9b99107bebe1c7e22fd59",
  "seqno": 21,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgpWtOzFrpGR+35tPnmjQTiuhGxUm9lDvIGcok4z+r0n8Kp3BheWxvYWTESpcCFcQguTS8URSCWE3C8YoXjkX/YdMy2pinqbmRB76+HH4i/VnEIAXgjKbexPi6QeolUQGQMrJksirsmkXdYfhDjcc57Vo9AgHCo3NpZ8RABnIqU25pqIfgF5j40+5EMGOfhZPwOdxaWFRu+vzJcpsJ4cdunQvfNVZ9whnFMbHpDqePfybAw9HDhNrnBizoCahzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIL4QChPCkR+iipUsQ8BTb6zjW4mkisWozJenviA1oWfPo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/mmh

==================================================================
