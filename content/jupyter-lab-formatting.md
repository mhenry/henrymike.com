Title: Code Formatting in JupyterLab
Date: 2020-10-25 14:38:00
Modified: 2020-09-01 14:38:00
Category: python
Tags: python, jupyter, formatting
Slug: code-formatting-in-jupyterlab
Authors: Henry
Summary: Automatic code formatting in JupyterLab

I really enjoy using [`black`](https://black.readthedocs.io/en/stable/) and [`isort`](https://pycqa.github.io/isort/) to format code.
Both tools provide a command line interface and work well with python files.
In order to get them to work with Jupyter Notebooks, specifically JupyterLab, there are some additional steps to follow.

First, install [this jupyter labextension](https://jupyterlab-code-formatter.readthedocs.io/en/latest/index.html) (be sure to active your virtual environment first!) with:
```bash
jupyter labextension install @ryantam626/jupyterlab_code_formatter
pip install jupyterlab_code_formatter
jupyter serverextension enable --py jupyterlab_code_formatter
```

Then install the python code formatting tools of your choice:

```bash
pip install isort black
```

Then restart the JuypterLab if it is running, and you should be able to run `isort` and `black` by selecting cells, then right clicking and selecting `Format Cells`.
See the documentation [here](https://jupyterlab-code-formatter.readthedocs.io/en/latest/how-to-use.html) for more detailed instructions.  
