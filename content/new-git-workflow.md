Title: New Git Workflow
Date: 2020-09-09 14:56:55
Modified: 2020-09-09 14:56:55
Category: tools
Tags: git, tools, workflow
Slug: new-git-workflow
Authors: Henry
Summary: How to do partial file staging!

I use [git](https://git-scm.com/) everyday and I find invaluable. 
It gives me an infinite "undo" button when working on personal projects and allows me to collaborate with hundreds of developers on larger projects.
Git is not the most user-friendly software and has a lot of warts.
After using git professionally for five years, I've grown quite fond of it.
Lately I've been working on writing papers using LaTeX and I keep the source files under version control with git.
Sometimes when writing, I will be working on a section but notice a typo in a different section.
If I fix the typo after I've made some changes already, then my commit message would be suboptimal e.g. `Added new figure in Tg section, fixed typo in conclusion`.
Not the worst commit message in the world, but it would be nice to split the commit into two commits, but until recently I didn't know how to stage part of a file.
The [solution](https://git-scm.com/book/en/v2/Git-Tools-Interactive-Staging) is to use `git add --patch <filename>` or `git add -p <filename`.
This will open your text editor and git will show different "hunks" of the file and ask if you want to include that "hunk" in the staging.
This lest you stage (even non-contiguous) hunks for a commit.
Sometimes I forget exactly what I decided to stage so I've taken to use `git commit -pv` which will show you what you changed when writing the commit message.  
