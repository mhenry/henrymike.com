Title: Start of Career Search
Date: 2020-09-01 14:23:58
Modified: 2020-09-01 14:23:58
Category: python
Tags: career, phd
Slug: start-of-career-search
Authors: Henry
Summary: Start of my career search

I'm really close to graduating and I need to start looking for a job.
I've spent a bit of time polishing up my [CV]({filename}pages/cv.rst).
I'm using [reStructuredText](https://docutils.readthedocs.io/en/sphinx-docs/user/rst/quickstart.html) for my [CV]({filename}pages/cv.rst) and then use different tools to get into the correct format.
For PDF I've been using [rst2pdf](https://rst2pdf.org/) and have had good results.
I need to make a style sheet so that it looks nicer, but since I'm focusing mostly on academic jobs, I'm not too worried about it looking really nice.
That might be a bit naive, but right now I barely have time to work on my dissertation.
A pretty CV can wait once I have my PhD in hand.
