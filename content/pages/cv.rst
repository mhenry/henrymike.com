:slug: cv
:modified: 2023-11-14

Michael Henry, PhD
==================
+--------------------------------------------------------------+
| :Github: https://github.com/mikemhenry                       |
| :Linkedin: https://www.linkedin.com/in/mikemhenry            |
| :Blog: https://henrymike.com                                 |
| :Company: https://www.mkn7787.com                            |
| :OCID: https://orcid.org/0000-0002-3870-9993                 |
+--------------------------------------------------------------+

:Interests: Self-Assembly, Molecular Dynamics, Machine Learning,
            Scientific Software Engineering, Coarse Grain Models,
            Free Energy Perturbation, DevOps, HPC, Cloud Compute,
            Inclusive Science. Maintainer for NumFOCUS affiliated
            project, signac.

Technical skills
----------------

:Operating Systems: Unix/Linux, OSX, Windows.

:Coding: Python, C, C++, CUDA.

:Tools: Git, Bash, Vim, LaTeX.

:Teaching: Software Carpentry Instructor Trainer.

Work Experience
---------------

:2021 - Present: Software Scientist for https://openfree.energy

    * coda-forge packaging
    * QA & Benchmarking of our FEP pipeline
    * Developed docker and singularity/apptainer CD pipeline

:2020 - Present: Software Scientist for https://www.choderalab.org/

    * Continuous integration (CI) on AWS EC2 GPU instances with Github actions
    * Experience in the OpenMM & OpenFF scientific software ecosystem
    * Folding@home integration for FEP simulations

:2020 - 2021: Software Scientist for https://mobleylab.org/

    * Containerized scientific workflows
    * Developed a Django web app for blind SAMPL challenges https://www.samplchallenges.org/

Education
---------

:2015-2020: PhD, Material Science and Engineering, Boise State University, Boise, Idaho.

:2015-2018: MEng, Material Science and Engineering, Boise State University, Boise, Idaho.

:2010-2014: Bachelor of Arts in Math and Physics, Simpson College, Indianola, Iowa.

Teaching Experience
-------------------

:2019: Software Carpentry Instructor Trainer

    * Trainer for 3 instructor training workshops 

:2018: Software Carpentry Instructor
    
    * Instructor for 11 workshops

:2018: TA for MSE 150 - Computational  Tools for Material Science
    
    * Course adapted Software Carpentry Bash, Git, Python lessons into the material science domain

:2017-2019: AIChE Computational Molecular Science and Engineering Forum Hands-On Workshop
    
    * Created virtual machine for workshop, integrating software from multiple research groups

    * Supported instructors with tutorial development and testing

:2016: TA for MSE 245 - Introduction to Material Science

Research Experience
-------------------

:2020 - Present: Free Energy Perturbation (FEP)

    * FEP pipeline for structure based drug discovery project
    * Benchmarks and best practices curation

:2016 - 2020: M1 Catalyst Surface Reactions

    * Collaboration with Idaho National Laboratory to develop
      new molecular dynamics models of M1 catalyst surface 
      reactions with organic chemicals

:2016 - 2020: Quantifying Microstructural Sensitivity to Processing Protocols, Boise State University

    * Collaboration with Boeing’s Next Generation Composite Materials team 
      to develop computational tools to improve epoxy curing protocols

:2015 - 2020: Modeling Material Self-Assembly, Boise State University

    * Studying self-assembly of semi-conducting polymers for organic photovoltaic applications

    * Developing coarse-grained molecular models

    * High throughput material screening

:2013 - 2014: Steller Collapse, Simpson College

    * Developed a program in Python to simulate stellar 
      collapse using a 1-D ising model.

:2013 Summer: Giant Monopole Resonance, Texas A&M

    * 10 week long REU at the Cyclotron Institute   

    * Developed a program in visual basic for Excel 
      to analyze experimental data 

:2013: DNA Origami, Simpson College

    * Member of research collaboration at Simpson 
      College studying DNA origami.

    * Worked on synthesis and design of DNA origami 
      structures.

    * Characterized structures with atomic force 
      microscopy.

:2011 Summer: Ramsey's theorem, Simpson College

    * Developed Genetic algorithms to improve the 
      lower bound on the r(5,5) Ramsey Number.

:2011 - 2013: Rb-Ar Collisions, Simpson College

    * Studied Rb-Ar collisions using laser spectroscopy.
      
    * Constructed an external cavity diode laser.

Publications 
------------

Boby, M. L.; Fearon, D.; Ferla, M.; Filep, M.; Koekemoer, L.; Robinson, M. C.; Chodera, J. D.; Lee, A. A.; London, N.; von Delft, A.; et al. Open Science Discovery of Potent Noncovalent SARS-CoV-2 Main Protease Inhibitors. Science, 2023, 382. https://doi.org/10.1126/science.abo7201.

Eastman, P.; Galvelis, R.; Peláez, R. P.; Abreu, C. R. A.; Farr, S. E.; Gallicchio, E.; Gorenko, A.; Henry, M. M.; Hu, F.; Huang, J.; et al. OpenMM 8: Molecular Dynamics Simulation with Machine Learning Potentials. arXiv 2023. https://doi.org/10.48550/ARXIV.2310.03121. 

Zhang, I.; Rufa, D. A.; Pulido, I.; Henry, M. M.; Rosen, L. E.; Hauser, K.; Singh, S.; Chodera, J. D. Identifying and Overcoming the Sampling Challenges in Relative Binding Free Energy Calculations of a Model Protein:Protein Complex, 2023. https://doi.org/10.1101/2023.03.07.530278.

Henry, M.M.; Thomas, S.; Alberts, M.; Estridge, C.E.; Farmer, B.; McNair, O.; Jankowski, E. General-Purpose Coarse-Grained Toughened Thermoset Model for 44DDS/DGEBA/PES. Polymers 2020, 12, 2547. https://doi.org/10.3390/polym12112547

Jankowski, E., Ellyson, N., Fothergill, J. W., Henry, M. M., Leibowitz, M. H.,  Miller, E. D., Alberts, M., Chesser, S., Guevara, J. D., Jones, C. D., Klopfenstein, M.,Noneman, K. K.,Singleton, R., Uriarte-Mendoza, R. A.,Thomas, S., Estridge, C. E., & Jones, M. L. "Perspective on Coarse-Graining, Cognitive Load, and Materials Simulation". Comp. Mater. Sci.169, 109129, 2019.

Miller, E. D., Jones, M.L., Henry, M. M., Stanfill, B., & Jankowski, E. "Machine learning predictions of electronic couplings for charge transport calculations of P3HT". AIChE J., 2019.

Miller, E. D., Jones, M.L., Henry, M. M., Chery, P, Miller, K., & Jankowski, E. "Optimization and Validation of Efficient Models for Predicting Polythiophene Self-Assembly". Polymers 10, 1305, 2018.

Thomas, S., Alberts, M., Henry, M., M., Estridge, C., and Jankowski, E. "Routine million-particle simulations of epoxy curing with dissipative particle dynamics." J. Theor. Comput. Chem.17, 1840005, 2018.

Henry M. M., Jones, M. L., Oosterhout, S. D., Braunecker, W. A., Kemper, T. W., Larsen, R. E., Kopidakis, N, Jankowski, E. Simplified Models for Accelerated Structural Prediction of Conjugated Semiconducting Polymers. The Journal of Physical Chemistry C, 121(47), 26528–26538. 2017.

Leibowitz, Michael Henry, Miller, E. D., & Henry, M. M. Application of artificial neural networks to identify equilibration in computer simulations. J. Phys. Conf. Ser.921, 012013, 2017.

GJ Kumbartzki, N Benczer-Koller, S Burcher, A Ratkiewicz, SL Rice, YY Sharon, L Zamick, K-H Speidel, DA Torres, K Sieja, M McCleskey, A Cudd, M Henry, A Saastamoinen, M Slater, A Spiridon, S Yu Torilov, VI Zherebchevsky, G Gürdal, SJQ Robinson, SD Pain, JT Burke, "Transition from collectivity to single-particle degrees of freedom from magnetic moment measurements on 38 82 sr 44 and 38 90 sr 52," Physical Review C, vol. 89, no. 6, p. 064305, 2014.

Mike M. Henry and D. M. Cates, "Riemann and euler sum investigation in an introductory calculus class" Open Journal of Discrete Mathematics, vol. 1, no. 2, pp. 50–61, 2011.

Presentations
-------------

Henry, M. M. and Jankowski, E. "Automating Transferable Coarse-Model Generation and Back-Mapping with an Open Source Toolchain" AIChE Annual Meeting, 2019.

Mike Henry, "Long-Time Molecular Simulations for Linking Organic Semiconductor Morphologies to Carrier Mobilities" AIChE 2018 Annual Metting, Pittsburgh Pennsylvania, 2018.

Mike Henry, Evan Miller, Matthew Jones, and Eric Jankowski, "Predicting Charge Transport Properties in Semiconducting Polymers" Foundations of Molecular Modeling and Simulation, Delavan, Wisconsin, 2018. 

Henry M., Jones, M. L., Jankowski, E. "Predicting charge transport properties in organic photovoltaic devices with coarse grained models" APS March Meeting, Los Angeles, 2018.

Carla Estridge, Michael Henry, Monet Alberts, Eric Jankowski, "Effect of Cure Process Pathway on Thermoset Resin" APS March Meeting, New Orleans, Louisiana, 2017. 

Michael Henry and Eric Jankowski, "Computational and Kinetic Considerations for Morphology Prediction of Donor-Acceptor Oligomers for Organic Photovoltaics" AIChE 2016 Annual Meeting, San Francisco, California, 2016. 

Mike Henry and Dr. Eric Jankowski, "Predicting Morphology of Donor/Acceptor Oligomers for Organic Photovoltaics with Molecular Dynamics Simulations" Foundations of Molecular Modeling and Simulation, Mt. Hood, Oregon, 2015. 

Mike Henry, "Death of a Star", 2014 Honors Symposium, Simpson College, Iowa, 2014

Mike Henry, "Determining Physical Continuum Background Using the Calculated Cross-Section of the Giant Monopole Resonance", Division of Nuclear Physics Conference, Newport News, Virginia, 2013.

Mike Henry, Heather Malbon, and Max Nguyen, "Finding Ramsey Numbers", 2012 Honors Symposium, Simpson College, Iowa, 2012

Mike Henry, Heather Malbon, and Max Nguyen, "Finding Ramsey Numbers", Mathfest, Lexington, Kentucky, 2011

Mike Henry, "Velocity Redistribution and Hyperfine State Changing Collisions in Rb-Ar Collisions", Iowa Academy of Science, Wartburg College, Iowa, 2011.
    
Mike Henry, "Velocity Redistribution and Hyperfine State Changing Collisions in Rb-Ar Collisions", 2011 Honors Symposium, Simpson College, Iowa, 2011.

Awards and Honors
-----------------
:2020: Micron School of Material Science and Engineering "Graduate Student of the Year"

:2019: Boise State Representative in Statewide Three Minute Thesis

:2018: AIChE Area 8E Graduate Award Finalist

:2018: Micron School of Material Science and Engineering "Graduate student teaching award"

:2018: Three Minute Thesis (3MT®) State Finalist

:2018: AIChE Area 8E Graduate Award Finalist

:2018: Micron School of Material Science and Engineering "Teaching Award"

:2017: Three Minute Thesis (3MT®) "Audience Choice" $300 prize

    * "Participants have just three minutes and one slide to present compelling oration on their thesis or dissertation topic and its significance."

:2017: Micron School of Material Science and Engineering "Volunteer of the Year"

:2016 - 2017: NASA Idaho Space Grant Consortium (ISGC) Fellowship $18,000

    * ISGC fellowships support graduate students who are conducting thesis or dissertation research in a STEM field, particularly those with an interest in NASA or aerospace-related topics.

:2015: International High Performance Computing Summer School

    * Week long summer school with expenses paid, sponsored by XSEDE, PRACE, and RIKEN, Toronto CA.
    
:2014: Meritorious Award, The Interdisciplinary Contest in Modeling

:2014: Sigma Pi Sigma, National Physics Honor Society, 
    
    * Top third of class with physics GPA of at least 3.5

:2014: Outstanding Senior in Physics, Simpson College

:2013: Midwest Instruction and Computing Symposium (MICS) Programing Contest 3rd Place

:2012: Bronze Medal, University Physics Competition

:2010 - 2014: Honor Scholarship, Simpson College

    * Awarded to students possessing outstanding academic ability as evidenced by high school performance and success on the American College Test (ACT)
