Title: Push to a Remote Branch With a Different Name
Date: 2021-02-10 10:06:07
Modified: 2021-02-10 10:06:07
Category: tools
Tags: git, github
Slug: push-different-name
Authors: Henry
Summary: Pushing a branch to a remote that has a different name than your local branch.

Sometimes I use the github web UI to make simple fixes and don't bother to rename the branch it automatically creates for the PR.
These simple fixes often turn into larger fixes and I would prefer to work locally instead of with the web UI.
When working locally, I'm more likely to care and make a better branch name.
Fortunately git supports pushing to a branch with a different syntax:

``` bash
git push <REMOTE_NAME> <LOCAL_BRANCH>:<REMOTE_BRANCH>
```

Full example:

``` bash
git push upstream feat/fix_env_and_openff_rename:mikemhenry-patch-1
```
