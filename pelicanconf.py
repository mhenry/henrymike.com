#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Mike Henry'
SITENAME = 'Mike Henry'
SITEURL = ''

THEME = 'themes/pelican-bootstrap3'
BOOTSTRAP_THEME = 'cyborg'

JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
PLUGIN_PATHS = ['pelican-plugins', ]
PLUGINS = ['i18n_subsites', ]
PATH = 'content'
I18N_TEMPLATES_LANG = 'en'
CC_LICENSE = "BY-SA"
TIMEZONE = 'America/Boise'
PYGMENTS_STYLE = 'solarizeddark'
DEFAULT_LANG = 'en'

STATIC_PATHS = (
    'static',
)
EXTRA_PATH_METADATA = {
    'static/robots.txt': {'path': 'robots.txt'},
    'static/favicon.ico': {'path': 'favicon.ico'},
}

DIRECT_TEMPLATES = ('index', 'tags', 'categories', 'archives', 'sitemap')
SITEMAP_SAVE_AS = 'sitemap.xml'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Handy Code Job', 'https://www.handycodejob.com/'),
         ('CV', 'https://www.henrymike.com/pages/cv.html'),)

# Social widget
SOCIAL = (('twitter', 'https://twitter.com/mikehenry42'),
          ('linkedin', 'https://www.linkedin.com/in/mikemhenry'),
          ('github', 'https://github.com/mikemhenry'),
          ('gitlab', 'https://gitlab.com/mhenry'),
          ('keybase', 'https://keybase.io/mmh'),)

DEFAULT_PAGINATION = 10
OUTPUT_PATH = 'public/'
# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
